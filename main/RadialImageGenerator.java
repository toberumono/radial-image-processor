package main;

import structures.Image;

public class RadialImageGenerator {
	
	/**
	 * @param input
	 *            an image to convert to a radial format
	 * @param numSlices
	 *            the number of radial divisions in a quarter of the image
	 * @param numRings
	 *            the number of rings to divide each slice in to
	 * @return the radial sectors without any color conversions.<br>
	 *         This is equivalent to calling {@link #generateRadialSectors(Image, int, int, PixelColorConverter)
	 *         generateRadialSectors(input, numSlices, numRings, null)}
	 */
	public static Sector[][] generateRadialSectors(Image input, int numSlices, int numRings) {
		return generateRadialSectors(input, numSlices, numRings, null);
	}
	
	/**
	 * @param input
	 *            an image to convert to a radial format
	 * @param numSlices
	 *            the number of radial divisions in a quarter of the image
	 * @param numRings
	 *            the number of rings to divide each slice in to
	 * @param colorConverter
	 *            a filter to apply to each sector
	 * @return the radial sectors with the color conversion filter applied
	 */
	public static Sector[][] generateRadialSectors(Image input, int numSlices, int numRings, PixelColorConverter colorConverter) {
		numSlices *= 4;
		int numBands = input.getImage().getRaster().getNumBands(), center[] = {input.getWidth() / 2, input.getHeight() / 2};
		double angle = (2 * Math.PI) / (numSlices), ringWidth = ((center[0] > center[1]) ? center[0] : center[1]) / numRings, maxRadius = numRings * ringWidth;
		
		Sector[][] sectors = new Sector[numSlices][numRings]; //Each of the radial sectors.  This is of the form [(int)(angle from x-axis / angle per sector)][(int)(radius from center/radius per ring)]
		
		//Initialize the sectors
		for (int i = 0; i < numSlices; i++)
			for (int j = 0; j < numRings; j++)
				sectors[i][j] = new Sector(numBands);
		
		for (int i = 0; i < input.getWidth(); i++) {
			for (int j = 0; j < input.getHeight(); j++) {
				double radius = Math.sqrt(Math.pow(i - center[0], 2) + Math.pow(j - center[1], 2));
				if (radius >= maxRadius) //If the radius puts this pixel outside of the largest ring.
					continue;
				double theta = Math.acos((i - center[0]) / radius);
				
				int slice = ((j > center[1]) ? 0 : sectors.length / 2) + (int) (theta / angle); //If the pixel is in the negative y, increase the sector index appropriately.
				if (slice == sectors.length)
					slice--;
				sectors[slice][(int) (radius / ringWidth)].addPixel(input.readPixel(i, j), i, j); //This determines where in the sector map this pixel falls, and stores it there.
			}
		}
		
		if (colorConverter != null)
			for (int i = 0; i < sectors.length; i++)
				for (int j = 0; j < sectors[i].length; j++)
					if (sectors[i][j] != null)
						sectors[i][j].convert(colorConverter);
		
		System.out.println("Completed processing with " + numSlices + " total slices and " + numRings + " rings, for a total of " + (numSlices * numRings) + " sectors.");
		return sectors;
	}
}
