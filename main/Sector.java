package main;

import java.util.ArrayList;

/**
 * Stores the total channel values for some set of pixels and the location of those pixels in the image.
 * 
 * @author Joshua Lipstone
 */
public class Sector {
	private int[] channels;
	private ArrayList<Pair> pixels;
	
	/**
	 * Constructs an empty sector with the specified number of bands or channels
	 * 
	 * @param numBands
	 *            the number of bands that the pixels in this <tt>Sector</tt>
	 */
	public Sector(int numBands) {
		channels = new int[numBands];
		pixels = new ArrayList<>();
	}
	
	public void addPixel(int[] pixel, int x, int y) {
		if (pixel.length != channels.length)
			return;
		for (int i = 0; i < channels.length; i++)
			channels[i] = channels[i] + pixel[i];
		pixels.add(new Pair(x, y));
	}
	
	/**
	 * @return the net channel values of this sector
	 */
	public int[] getChannels() {
		if (pixels.size() == 0 || pixels.size() == 1)
			return channels;
		int[] output = new int[channels.length];
		for (int i = 0; i < channels.length; i++)
			output[i] = channels[i] / pixels.size();
		return output;
	}
	
	/**
	 * @return the average value of the channels in this pixel. This is useful for conversions between image types
	 */
	public int getChannelAverage() {
		int channelSum = 0;
		int[] channels = getChannels();
		for (int i = 0; i < channels.length; i++)
			channelSum += channels[i];
		return channelSum / channels.length;
	}
	
	/**
	 * @return all the pixels in this sector
	 */
	public ArrayList<Pair> getPixels() {
		return pixels;
	}
	
	/**
	 * Filters this image with the algorithm specified in the <tt>PixelColorConverter</tt>
	 * 
	 * @param pixelColorConverter
	 *            the conversion method to use
	 */
	public void convert(PixelColorConverter pixelColorConverter) {
		int[] channels = pixelColorConverter.convert(getChannels());
		for (int i = 0; i < channels.length; i++)
			this.channels[i] = channels[i] * pixels.size();
	}
	
}
