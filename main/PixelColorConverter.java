package main;

public abstract class PixelColorConverter {
	
	/**
	 * Performs some operation or set of operations on the color channels for a given pixel in an image
	 * 
	 * @param channels
	 *            the channels from the original image
	 * @return the channels after the operation or operations have been performed on them
	 */
	public abstract int[] convert(int[] channels);
}
