package testHarnesses;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import main.PixelColorConverter;
import main.RadialImageGenerator;
import main.Sector;
import structures.Image;

public class ArduinoRadialGenerator {
	static int averagePixelValue = 0;
	
	public static void main(String[] args) {
		try {
			Image image = new Image("/Users/joshualipstone/Pictures/Text.jpg");
			File sectorFile = new File(image.getBaseName() + "_sectors.txt");
			if (!sectorFile.exists())
				sectorFile.createNewFile();
			Sector[][] sectors = RadialImageGenerator.generateRadialSectors(image, 80, 32);
			int minSum = 256, maxSum = 0;
			for (int i = 0; i < sectors.length; i++) {
				for (int j = 0; j < sectors[i].length; j++) {
					int channelSum = sectors[i][j].getChannelAverage();
					if (channelSum < minSum)
						minSum = channelSum;
					if (channelSum > maxSum)
						maxSum = channelSum;
				}
			}
			averagePixelValue = (maxSum + minSum) / 2;
			
			ArrayList<String> allBits = new ArrayList<>();
			
			if (sectorFile.canWrite()) {
				BufferedWriter sectorOut = new BufferedWriter(new FileWriter(sectorFile));
				for (int i = 0; i < sectors.length; i++) {
					String bits = "";
					for (int j = 0; j < sectors[i].length; j++) {
						if (sectors[i][j].getChannelAverage() < averagePixelValue)
							bits = bits + "0";
						else
							bits = bits + "1";
					}
					allBits.add(bits);
					sectorOut.write(bits + "\n");
				}
				sectorOut.flush();
				sectorOut.close();
			}
			
			File programFile = new File("/Users/joshualipstone/Dropbox/Arduino Projects/motor_test/motor_test.ino");
			String program = "";
			BufferedReader programReader = new BufferedReader(new FileReader(programFile));
			while (programReader.ready())
				program = program + programReader.readLine() + "\n";
			String beginLine = "/*BEGIN IMAGE ARRAY*/", endLine = "/*END IMAGE ARRAY*/";
			
			String insert = "unsigned char imageArray[" + allBits.size() + "][" + allBits.get(0).length() + "] = {\n";
			for (int i = 0; i < allBits.size(); i++) {
				insert = insert + "{";
				for (char c : allBits.get(i).toCharArray())
					insert = insert + c + ", ";
				insert = insert.substring(0, insert.length() - 2) + "},\n";
			}
			if (insert.length() > 1)
				insert = insert.substring(0, insert.length() - 2) + "};\n";
			
			programReader.close();
			
			program = program.substring(0, program.indexOf(beginLine) + 22) + insert + program.substring(program.indexOf(endLine));
			
			BufferedWriter programWriter = new BufferedWriter(new FileWriter(programFile));
			programWriter.write(program);
			programWriter.flush();
			programWriter.close();
			
			Image test = new Image(image, 0, 0, image.getWidth(), image.getHeight());
			BlackAndWhiteConverter converter = new BlackAndWhiteConverter();
			for (int i = 0; i < sectors.length; i++) {
				for (int j = 0; j < sectors[i].length; j++) {
					sectors[i][j].convert(converter);
					for (int k = 0; k < sectors[i][j].getPixels().size(); k++)
						test.writePixel(sectors[i][j].getPixels().get(k).x, sectors[i][j].getPixels().get(k).y, sectors[i][j].getChannels());
				}
			}
			
			try {
				test.save("test");
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		catch (IOException e) {}
	}
	
	static class BlackAndWhiteConverter extends PixelColorConverter {
		
		@Override
		public int[] convert(int[] channels) {
			int[] output = new int[channels.length];
			int sum = 0;
			for (int i = 0; i < channels.length; i++)
				sum += channels[i];
			sum /= channels.length;
			if (sum > averagePixelValue)
				for (int i = 0; i < output.length; i++)
					output[i] = 255;
			return output;
		}
		
	}
}